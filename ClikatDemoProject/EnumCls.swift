//
//  EnumCls.swift
//  ClikatDemoProject
//
//  Created by Sierra 4 on 15/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation

enum tableCell: String
{
    case advertisement = "UpperTableCell"
    case categories =  "MiddleTableCell"
    case OffrRecom = "ScndMiddleTableCell"
    case lower = "LowerTableCell"
    var allValue:String
    {
        return self.rawValue
    }

}
enum tableCellIdentifers : String
{
    case advertisement = "UpperCell"
    case categories =  "MiddleCell"
    case OffrRecom = "ScndMiddleCell"
    case lower = "LowerCell"
    var allValue:String
    {
        return self.rawValue
    }
}
enum CollecCellIdentifers: String
{
    case advertisement = "UpperCollnCell"
    case categories =  "MiddleColncell"
    case OffrRecom = "SndMiddleCollnCell"
    case lower = "LowerCollecnCell"
    var allValue:String
    {
        return self.rawValue
    }
    
}
enum collecnView : String
{
    case advertisement = "UpperCollectionViewCell"
    case categories =  "MiddleCollnViewCell"
    case OffrRecom = "ScndMiddleCollnViewCell"
    case lower = "LowerCollnViewCell"
    var allValue:String
    {
        return self.rawValue
    }
}

enum Json : String{
    case areaId = "areaId"
    case areaIdString = "201"
    case countryId = "countryId"
    case countryIdString = "1"
   var  JsonValue: String
    {
        return self.rawValue
    }
}

enum slidrIdfr : String
{
    case sliderIdentifier = "sliderIdentifier"
    var allValue:String
        {
            return self.rawValue
    }
}
enum header : String
{
    case Offer = "Offer"
   case Recommended = "Recommended"
   case AvenirNext = "Avenir next"
   case headerXib = "headerXib"
   case MyAccount =   "My Account"
    var allValue:String
    {
        return self.rawValue
    }
}


enum slider : String
{
    case Home = "Home"
   case LiveSupport = "Live support"
   case Cart = "Cart"
    case Promotions = "Promotions"
    case Notifications = "Notifications"
   case CompareProducts = "Compare Products"
   case Myfavorites = "My favorites"
    case PendingOredrs = "Pending Oredrs"
   case ScheduledOrders = "Scheduled Orders"
   case Rateoredr = "Rate my oredr"
    case Orderhistory = "Order history"
    case Loyalitypoints = "Loyality points"
   case Shareapp = "Share app"
   case TermsConditions = "Terms & Conditions"
   case About  = "About Us"
    case Setings = "Setings"
    var allValue:String
    {
        return self.rawValue
    }
}

enum constants: String
{
    case error = "error"
    case urll = "https://appbean.clikat.com/get_all_category"
    var allValue:String
    {
        return self.rawValue
    }
}




