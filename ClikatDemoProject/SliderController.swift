//
//  SliderController.swift
//  ClikatDemoProject
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class SliderController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var UserImage: UIImageView!
    var headerSection : HeaderController?
    @IBOutlet weak var SliderviewOutlt: UIView!
    
    var arrOptions = [[SliderObjects(image:#imageLiteral(resourceName: "ic_home") ,name:slider.Home.allValue ),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_dashboard"),name:slider.LiveSupport.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_shopping_cart"),name:slider.Cart.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_favorite"),name:slider.Promotions.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_favorite"),name:slider.Notifications.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.CompareProducts.allValue)],
                      
                      [SliderObjects(image:#imageLiteral(resourceName: "ic_favorite"),name:slider.Myfavorites.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.PendingOredrs.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_dashboard") ,name:slider.ScheduledOrders.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_history"),name:slider.Rateoredr.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_history"),name:slider.Orderhistory.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.Loyalitypoints.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.Shareapp.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.TermsConditions.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.About.allValue),
                       SliderObjects(image:#imageLiteral(resourceName: "ic_build"),name:slider.Setings.allValue)]]
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        UserImage.layer.cornerRadius = UserImage.frame.height/2
        if !UIAccessibilityIsReduceTransparencyEnabled()
        {
            self.SliderviewOutlt.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.SliderviewOutlt.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view.addSubview(blurEffectView) 
            self.view.sendSubview(toBack: blurEffectView)
        }
        else
        {
            
        }
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        Back.backPanel(obj: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions[section].count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell:sliderTableViewCell = tableView.dequeueReusableCell(withIdentifier: slidrIdfr.sliderIdentifier.allValue , for: indexPath) as? sliderTableViewCell
            else
        {
            return sliderTableViewCell()}
        cell.Object = arrOptions[indexPath.section][indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: header.headerXib.allValue , bundle: nil)
        let headerView = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderController
        headerView?.headerLbel1.textColor = UIColor.white
        headerView?.headerLbel1.font = UIFont(name: (headerView?.headerLbel1.font.fontName)!, size: 14.0)
        headerView?.headerLbel1.text = header.MyAccount.allValue
        headerView?.VButonOutlt.isHidden = true
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0)
        {
            return 0
        }else{
            return 25
        }
    }
}

