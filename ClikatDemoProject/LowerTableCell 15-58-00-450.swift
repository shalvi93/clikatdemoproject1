//
//  LowerTableCell.swift
//  
//
//  Created by Sierra 4 on 08/03/17.
//
//

import UIKit

class LowerTableCell: UITableViewCell , UICollectionViewDelegate , UICollectionViewDataSource {

    @IBOutlet weak var lowerCollnOutlt: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        let nibScndMiddleTableCell = UINib(nibName: "LowerCollnViewCell", bundle: nil)
//        lowerCollnOutlt.register (nibScndMiddleTableCell, forCellWithReuseIdentifier: "LowerCollecnCell")
         lowerCollnOutlt.delegate = self
         lowerCollnOutlt.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    


func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 2
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let lowerTableCel = collectionView.dequeueReusableCell(withReuseIdentifier: CollecCellIdentifers.lower.allValue, for: indexPath) as! LowerCollnViewCell
    return lowerTableCel
}
}
