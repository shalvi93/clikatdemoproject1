//
//  ViewController.swift
//  ClikatDemoProject
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import ObjectMapper

class ViewController: UIViewController {
    var userModel :DataClass?
    var ModelIns: DataClass?
    @IBOutlet weak var tblViewOutlet: UITableView!
    var headerSection : HeaderController?
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        tblViewOutlet.delegate = self
        tblViewOutlet.dataSource = self
        tblViewOutlet.rowHeight = UITableViewAutomaticDimension
        tblViewOutlet.estimatedRowHeight = 200
        
        let nibUpper = UINib(nibName: tableCell.advertisement.allValue, bundle: nil)
        tblViewOutlet.register(nibUpper, forCellReuseIdentifier: tableCellIdentifers.advertisement.allValue)
        
        let nibMiddle = UINib(nibName: tableCell.categories.allValue, bundle: nil)
        tblViewOutlet.register( nibMiddle, forCellReuseIdentifier: tableCellIdentifers.categories.allValue)
        
        let nibSndMiddle = UINib(nibName:  tableCell.OffrRecom.allValue, bundle: nil)
        tblViewOutlet.register(nibSndMiddle, forCellReuseIdentifier:tableCellIdentifers.OffrRecom.allValue )
        
        let nibLower = UINib(nibName: tableCell.lower.allValue, bundle:nil)
        tblViewOutlet.register(nibLower, forCellReuseIdentifier: tableCellIdentifers.lower.allValue)
    }
    
    
    func fetchData() {
        let param:[String:Any] = [ Json.areaId.JsonValue  : 201 , Json.countryId.JsonValue: 1 ]
        
        ApiHandler.fetchData(parameters: param) {
            (jsonData) in
            self.ModelIns = Mapper<DataClass>().map(JSONObject: jsonData)
            self.tblViewOutlet.reloadData()
            print(self.userModel?.message ?? "")
            print(jsonData!)
            
        }
    }
    
    
    @IBAction func sliderButton(_ sender: Any) {
        Appearance.ShowPanel(obj: self, identifier: slidrIdfr.sliderIdentifier.allValue )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell()
        if (indexPath.section == 0 )
        {
            let UpperCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifers.advertisement.allValue, for: indexPath) as! UpperTableCell;
            return UpperCell
            
        }
        else if (indexPath.section == 1)
        {
            let MiddleCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifers.categories.allValue, for: indexPath) as! MiddleTableCell;
            MiddleCell.detailsOfCategory = self.ModelIns?.idata?.english
            MiddleCell.middleCollnOutlet.reloadData()
            return MiddleCell
        }
        else if ((indexPath.section == 2) || (indexPath.section == 3))
        {
            let ScndMiddleCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifers.OffrRecom.allValue , for: indexPath) as! ScndMiddleTableCell;
            ScndMiddleCell.tableSection = indexPath.section
            return ScndMiddleCell
            
        }
        else{
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 1)
        { return 6 * 183}
        else{ return UITableViewAutomaticDimension}
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib(nibName: header.headerXib.allValue, bundle: nil)
        let headerSection = nib.instantiate(withOwner: nil, options: nil)[0] as? HeaderController
       
        if(section == 2){
            headerSection?.headerLbel1.font = UIFont(name: (headerSection?.headerLbel1.font.fontName)!, size: 13)
            headerSection?.headerLbel1.text = header.Offer.allValue
            headerSection?.VButonOutlt.titleLabel!.font =  UIFont(name: header.AvenirNext.allValue, size: 13)
            headerSection?.VButonOutlt.isHidden = false
            return headerSection
        }
        else{
            headerSection?.headerLbel1.font = UIFont(name:(headerSection?.headerLbel1.font.fontName)!, size: 13)
            headerSection?.headerLbel1.text = header.Recommended.allValue
            headerSection?.VButonOutlt.isHidden = true
            return headerSection
        }
        }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2 || section == 3)
        {
            return 30
        }
        else
        {
            return 0
        }
    }
    
}
