//
//  sliderTableViewCell.swift
//  ClikatDemoProject
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class sliderTableViewCell: UITableViewCell {

    @IBOutlet weak var SliderImage: UIImageView!
    @IBOutlet weak var SliderLabel: UILabel!

    
    var Object : SliderObjects? {
        didSet
        {
            updateUI()
        }
    }
    
    fileprivate func updateUI(){
        
        SliderImage?.image = Object?.image
        SliderLabel.text = Object?.name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
