//
//  ScndMiddleTableCell.swift
//  ClikatDemoProject
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ScndMiddleTableCell: UITableViewCell, UICollectionViewDelegate , UICollectionViewDataSource  {

    @IBOutlet weak var scdMidleCollcnOutlt: UICollectionView!
   
    var tableSection:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nibScndMiddleTableCell = UINib(nibName: collecnView.OffrRecom.allValue, bundle: nil)
        scdMidleCollcnOutlt.register (nibScndMiddleTableCell, forCellWithReuseIdentifier: CollecCellIdentifers.OffrRecom.allValue)
        
        scdMidleCollcnOutlt.delegate = self
        scdMidleCollcnOutlt.dataSource = self
        
        let nibScndLowerTableCell = UINib(nibName: collecnView.lower.allValue, bundle: nil)
        scdMidleCollcnOutlt.register (nibScndLowerTableCell, forCellWithReuseIdentifier: CollecCellIdentifers.lower.allValue)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    if(tableSection ==  2)
    {
    let UpperTableCel = collectionView.dequeueReusableCell(withReuseIdentifier: CollecCellIdentifers.OffrRecom.allValue, for: indexPath) as! ScndMiddleCollnViewCell
    return UpperTableCel    }
    else
    {
        let UpperTableCel = collectionView.dequeueReusableCell(withReuseIdentifier: CollecCellIdentifers.lower.allValue, for: indexPath) as! LowerCollnViewCell
        return UpperTableCel
    }
}
}
